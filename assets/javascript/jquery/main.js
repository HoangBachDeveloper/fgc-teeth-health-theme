$(function () {
    $('.home__news-slide').slick(
        {
            slidesToShow: 1,
            // slidesToScroll: 2,
            adaptiveHeight: true,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: $('#prev-btn'),
            nextArrow: $('#next-btn')
        }
    );
    $('.home__feedback-slide').slick(
        {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: $('#feedback-prev'),
            nextArrow: $('#feedback-next')
        }
    );
    $('.content-slide__wraps').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        swipe: true,
        swipeToSlide: true,
        // autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: $('#banner-slide-prev'),
        nextArrow: $('#banner-slide-next')
    });
    $('.product-slide__content--machine-chair').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_1'),
            nextArrow: $('.slide-control-next_1'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );
    $('.product-slide__content--machine-drill').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_1'),
            nextArrow: $('.slide-control-next_1'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );
    $('.product-slide__content--machine-compression').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_1'),
            nextArrow: $('.slide-control-next_1'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );
    $('.product-slide__content--machine-suck').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_1'),
            nextArrow: $('.slide-control-next_1'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );
    $('.product-slide__content--break-teeth-tool').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_2'),
            nextArrow: $('.slide-control-next_2'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );
    $('.product-slide__content--shape-tool').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_2'),
            nextArrow: $('.slide-control-next_2'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );
    $('.product-slide__content--anesthetize-tool').slick(
        {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            prevArrow: $('.slide-control-prev_2'),
            nextArrow: $('.slide-control-next_2'),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        }
    );


    $('.content__pagination ul li a').click(function (e) {
        e.preventDefault();
        $('.content__pagination ul li a.active').removeClass('active');
        $(this).addClass('active');
    });

    $('.hiring__sidebar-item').click(function (e) {
        e.preventDefault();
        $('.hiring__sidebar-item.active').removeClass('active');
        $(this).addClass('active');
    });

    // CLICK TO SEE THUMB IMG PRODUCT DETAILS
    $('.product__img-thumb ul li ').click(function (e) {
        e.preventDefault();
        $('.product__img-thumb ul li.active').removeClass('active');
        $(this).addClass('active');
    });
    $('.product__img-thumb ul li img').click(function (e) {
        e.preventDefault();
        console.log($(this).attr('src'))
        $('.product__img-main img').attr('src', $(this).attr('src'));
    });


    // RANGER SLIDER
    $(".js-range-slider").ionRangeSlider({
        type: "double",
        // grid: true,
        skin: "round",
        min: 0,
        max: 50000,
        from: 500,
        to: 40000,
        step: 1000,
        postfix: " vnđ"
    });
    $('#price-from').text($('.irs-from').text())
    $('#price-to').text($('.irs-to').text())
    $('.js-range-slider').change(function (e) {
        e.preventDefault();
        $('#price-from').text($('.irs-from').text())
        $('#price-to').text($('.irs-to').text())
    });
});